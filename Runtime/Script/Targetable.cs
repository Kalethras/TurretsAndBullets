﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Targetable : MonoBehaviour
{
    public static List<Targetable> targetInScene = new List<Targetable>();
    
    void OnEnable()
    {
        targetInScene.Add(this);
    }

     void OnDisable()
    {
        targetInScene.Remove( this);

    }
    public static Targetable GetNearestTarget(Vector3 position) {


        List<Targetable> targets = targetInScene.OrderBy(k => Vector3.Distance(position, k.transform.position)).ToList();
        if (targets.Count > 0) return targets[0];
        return null;
    }

    public static List<Targetable> GetOpponentsTargetInRegion(Vector3 from, float maxDistanceAllow, Allegiance_Tag.AllegianceType givenAllegiance)
    {
        List<Targetable> valideTarget = new List<Targetable>();
        GetOpponentsAllegianceTarget(givenAllegiance, ref valideTarget);
        valideTarget = GetTargetInRange(from, maxDistanceAllow, ref valideTarget);
        valideTarget = valideTarget.OrderBy(k => Vector3.Distance(from, k.transform.position)).ToList();
        return valideTarget;


    }

    private static List<Targetable>  GetTargetInRange(Vector3 from, float maxDistanceAllow, ref  List<Targetable> valideTarget)
    {
        List<Targetable> inRange = new List<Targetable>();
        for (int i = 0; i < valideTarget.Count; i++)
        {
            if (Vector3.Distance(valideTarget[i].transform.position, from) < maxDistanceAllow)
                inRange.Add(valideTarget[i]);

        }
       
       

        return inRange;
    }

    private static void GetOpponentsAllegianceTarget(Allegiance_Tag.AllegianceType givenAllegiance,ref  List<Targetable> valideTarget)
    {
        for (int i = 0; i < targetInScene.Count; i++)
        {
            Allegiance_Tag targAll = targetInScene[i].GetComponent<Allegiance_Tag>();
            if (targAll != null && targAll.GetAllegiance() != givenAllegiance)
            {
                valideTarget.Add(targetInScene[i]);
            }

        }
    }
}
