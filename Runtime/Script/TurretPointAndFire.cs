﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPointAndFire : MonoBehaviour
{
    public Allegiance_Tag m_alligance;
    public GameObject m_bulletPrefab;
    public Transform m_cannonPivot;
    public Transform m_whereToFireTheBullet;
    private Vector3 basePosition;
    public float m_maxRangeToFire = 5;
    public float fireTimer = 2f;
    private float reloadTimer = 0f;
    [Header("Debug")]
    public GameObject m_targetToFireOn;
    public Quaternion m_wantedRotation;
    public float m_lerpFactor = 0.1f;

    private void Start()
    {
        //gameObject.transform.LookAt(basePosition);
        InvokeRepeating("CheckForTarget",0,1);
    }

    public void CheckForTarget()
    {
        List<Targetable> targets = Targetable.GetOpponentsTargetInRegion(m_cannonPivot.position, m_maxRangeToFire, m_alligance.GetAllegiance());
        if (targets.Count < 1) {
            m_targetToFireOn = null;
        }
        else {
           
            m_targetToFireOn = targets[0].gameObject;
        }
    }

    void Update()
    {
        
        reloadTimer += Time.deltaTime;
        
        if (reloadTimer > fireTimer) {
            FireAtTarget();
            reloadTimer = 0;
        }

        
        if (m_targetToFireOn !=null)
        {
            m_wantedRotation = Quaternion.FromToRotation(Vector3.forward, m_targetToFireOn.transform.position - m_cannonPivot.position);
            m_cannonPivot.transform.rotation = Quaternion.Lerp(m_cannonPivot.transform.rotation, m_wantedRotation, Time.deltaTime* m_lerpFactor);
            //m_cannonPivot.transform.LookAt(m_targetToFireOn.transform);
        }
    }

   

 

    private void FireAtTarget()
    {
        if (m_targetToFireOn != null )
        {
            GameObject ammunition = Instantiate(m_bulletPrefab, m_whereToFireTheBullet.position, m_cannonPivot.transform.rotation);
            Allegiance_Tag myAllegiance = ammunition.GetComponent<Allegiance_Tag>();
            myAllegiance.SetAllegiance(m_alligance.GetAllegiance());
            
        }
    }
}
