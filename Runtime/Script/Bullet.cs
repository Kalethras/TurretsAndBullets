﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject hitPrefab;
    public GameObject muzzlePrefab;
    public float speed;
    public int dommage=1;
    public float m_lifeTime = 5;

    private void Start()
    {
        Destroy(this.gameObject,m_lifeTime);

        if (muzzlePrefab != null)
        {
            var muzzleVFX = Instantiate(muzzlePrefab, transform.position, Quaternion.identity);
            muzzleVFX.transform.forward = gameObject.transform.forward;
            var psMuzzle = muzzleVFX.GetComponent<ParticleSystem>();
            if (psMuzzle != null)
                Destroy(muzzleVFX, psMuzzle.main.duration);
            else
            {
                var psChild = muzzleVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(muzzleVFX, psChild.main.duration);
            }
        }
    }
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 pos = transform.position;

        if (other.GetComponent<Allegiance_Tag>() == null)
        {
            return;
        }

        if(other.GetComponent<Allegiance_Tag>().GetAllegiance() != gameObject.GetComponent<Allegiance_Tag>().GetAllegiance())
        {
            LifeIsLife killable = other.GetComponent<LifeIsLife>();
            if (killable) {
                killable.AddDommage(dommage);
                Destroy(gameObject);
            }

            if (hitPrefab != null)
            {
                var hitVFX = Instantiate(hitPrefab, pos, transform.rotation);
                var psHit = hitVFX.GetComponent<ParticleSystem>();
                if (psHit != null)
                    Destroy(hitVFX, psHit.main.duration);
                else
                {
                    var psChild = hitVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                    Destroy(hitVFX, psChild.main.duration);
                }
            }
        }


    }
}
