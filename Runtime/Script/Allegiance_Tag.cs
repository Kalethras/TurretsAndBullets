﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Allegiance_Tag : MonoBehaviour
{
    public enum AllegianceType { Player, Enemy }
    [SerializeField] AllegianceType myAllegiance;
    public AllegianceType GetAllegiance() { return myAllegiance; }
    public AllegianceType GetOppositeAllegiance() {
        if (myAllegiance == AllegianceType.Enemy)
            return AllegianceType.Player;
        else return AllegianceType.Enemy;
    }
    public void SetAllegiance(AllegianceType value) { myAllegiance = value; }
    public void SetAllegianceToAlly() { SetAllegiance(AllegianceType.Player); }
    public void SetAllegianceToEnemy() { SetAllegiance(AllegianceType.Enemy); }
    public void SwitchAllegiance() { SetAllegiance(GetOppositeAllegiance()); }
}

    
    
