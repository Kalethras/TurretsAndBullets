﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LifeIsLife : MonoBehaviour
{
    public float myHealth;
    public UnityEvent m_onDeath;
    public bool m_isDeath;

    
    public void AddDommage(int value) {
        myHealth -= value;
        if (myHealth <= 0) {
            if (!m_isDeath) {
                m_isDeath = true;
                m_onDeath.Invoke();
            }
        }
    }
}
