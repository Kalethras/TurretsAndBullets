﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpaceShip : MonoBehaviour
{
    [SerializeField] int armorToWin;
    public UnityEvent m_onWin;

    private void Start()
    {
        armorToWin = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Allegiance_Tag>() == null)
        {
            return;
        }
        if(other.GetComponent<Allegiance_Tag>().GetAllegiance() == gameObject.GetComponent<Allegiance_Tag>().GetAllegiance())
        {
            armorToWin++;
            if (armorToWin == 20)
            {
                //Do Win
            }
                
        }
    }
}
